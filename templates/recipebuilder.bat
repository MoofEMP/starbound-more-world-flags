@echo off

:loop
REM get user input
set /p object="Object name (e.g. newflagbahamas): newflag"
if %object%==q goto eof
set /p category="Continent name (europe, asia, africa, na, sa, misc): "

REM build flag.recipe
(
  echo;{
  echo;  "input" : [
  echo;    { "item" : "fabric", "count" : 10 }
  echo;  ],
  echo;  "output" : {
  REM this is changed
  echo;    "item" : "newflag%object%",
  echo;    "count" : 1
  echo;  },
  echo;  "groups" : [ "flagstation", "flagstation%category%" ]
  echo;}
) > "newflag%object%.recipe"

echo.
echo Recipe file created. Generate another, or enter q to exit.
echo.
goto loop

:eof