# Starbound More World Flags submod

WIP submod for [Combat Blob's World Flags](https://steamcommunity.com/sharedfiles/filedetails/?id=729433195) to, hopefully in time, add 550 or so new flags.

Current version: [**Beta 0.4.7 Hotfix 1**](CHANGELOG.md#047-hotfix-1-2021-06-09)

[`moreworldflagssubmod/`](moreworldflagssubmod) is the **MAIN MOD FOLDER.**  
&nbsp;&nbsp;&nbsp;&nbsp;This is what goes into `C:\Program Files (x86)\Steam\steamapps\common\Starbound\mods`. **That's all you need for installation.**

[`refs/`](refs) is a folder full of pdns containing flag refs, which will eventually be turned into ingame flags. This is done with [Flag Rippler for Paint.NET](../../../../../MoofEMP/pdn-flag-rippler).  
&nbsp;&nbsp;&nbsp;&nbsp;Refs which have already been turned into sprites are in [`refs/archived/`](refs/archived).  
[`templates/`](templates) is a few .pngs used to help create refs and turn them into sprites.  
&nbsp;&nbsp;&nbsp;&nbsp;[`templates/objectbuilder.bat`](templates/objectbuilder.bat) creates `flag.object` and `flag.frames` files for you!  
&nbsp;&nbsp;&nbsp;&nbsp;[`templates/recipebuilder.bat`](templates/recipebuilder.bat) will create the `flag.recipe` file as well.  
[`checklist.txt`](checklist.txt) is a gigantic list of all the flags I'd like to include in this submod. It's not exactly glamorous, so I have a prettier list just of every flag that has been fully implemented right here:

<details>
  <summary><b>List of flags</b> (250 items)</summary>
  
  - Afghanistan
  - Afghan Northern Alliance
  - Agender pride flag
  - Åland
  - Alabama
  - Alaska
  - Albania
  - Algeria
  - Andorra
  - Angola
  - Antigua and Barbuda
  - Aroace pride flag
  - Aromantic pride flag
  - Armenia
  - Armenian SSR
  - Artsakh
  - Aruba
  - Asexual pride flag
  - Atlantium
  - Austenasia
  - Austrian Empire
  - Azerbaijan
  - Azerbaijan SSR
  - Bahamas
  - Bahrain
  - Bangladesh
  - Barbados
  - Belarus
  - Byelorussian SSR
  - Belize
  - Benin
  - Bhutan
  - Bigender pride flag
  - Bisexual pride flag
  - Bolivia
  - Bornholm
  - Bornholm (alt)
  - Bosnia and Herzegovina (modern)
  - Republic of Bosnia and Herzegovina
  - Socialist Republic of Bosnia and Herzegovina
  - Botswana
  - Brunei
  - Burkina Faso
  - Burundi
  - Cambodia (modern)
  - Democratic Kampuchea (Communist Cambodia)
  - Cameroon
  - Cabo Verde
  - Carnatic
  - Central African Republic
  - Chad
  - Republic of China (modern)
  - Republic of China (1912)
  - Freetown Christiania
  - Colombia
  - Colorado
  - Comoros
  - Democratic Republic of the Congo
  - Republic of the Congo
  - Côte d'Ivoire
  - Croatia
  - Cuba
  - Czech Republic (tricolour)
  - Demiboy pride flag
  - Demigirl pride flag
  - Demiromantic pride flag
  - Demisexual pride flag
  - Naval ensign of Denmark
  - Djibouti
  - Elgaland-Vargaland
  - England
  - Estonia (nordic)
  - Estonian SSR
  - Faroe Islands
  - Fictosexual pride flag
  - Finsexual pride flag
  - Finsexual pride flag (alt)
  - Free France
  - Gabon
  - Gambia
  - Genderfluid pride flag
  - Genderqueer pride flag
  - Georgia
  - Georgian SSR
  - German Empire
  - Nazi Germany
  - Nazi Germany (alt)
  - East Germany
  - Ghana
  - Greece
  - Greece (alt)
  - Grey-asexual pride flag
  - Greyromantic pride flag
  - Guangxi Clique
  - Guernsey
  - Guinea
  - Guinea-Bissau
  - Holy Roman Empire
  - Icaria
  - Iceland
  - Naval ensign of Iceland
  - Intersex pride flag
  - Kingdom of Iraq
  - Jamaica
  - Jordan
  - Jutland
  - Karelo-Finnish SSR
  - Kazakh SSR
  - Kirghiz SSR
  - North Korea
  - Kekistan
  - Kuomintang Party
  - Kuwait
  - Ladonia
  - Laos
  - Latvia
  - Latvian SSR
  - Lesbian pride flag
  - LGBT pride flag
  - Libyan Arab Jamahiriya
  - Libya (modern)
  - Lithuania
  - Lithuanian SSR
  - Luxembourg
  - Madagascar
  - Mali
  - Malta
  - Manchukuo
  - Maratha Empire
  - Maritime Signal Alfa
  - Maritime Signal Bravo
  - Maritime Signal Charlie
  - Maritime Signal Delta
  - Maritime Signal Echo
  - Maritime Signal Foxtrot
  - Maritime Signal Golf
  - Maritime Signal Hotel
  - Maritime Signal India
  - Maritime Signal Juliet
  - Maritime Signal Kilo
  - Maritime Signal Lima
  - Maritime Signal Mike
  - Maritime Signal November
  - Maritime Signal Oscar
  - Maritime Signal Papa
  - Maritime Signal Quebec
  - Maritime Signal Romeo
  - Maritime Signal Sierra
  - Maritime Signal Tango
  - Maritime Signal Uniform
  - Maritime Signal Victor
  - Maritime Signal Whiskey
  - Maritime Signal Xray
  - Maritime Signal Yankee
  - Maritime Signal Zulu
  - Maritime Signal First Substitute
  - Maritime Signal Second Substitute
  - Maritime Signal Third Substitute
  - Maritime Signal Fourth Substitute
  - Mauritius
  - Mecklenburg
  - Mecklenburg-Vorpommern
  - Mengjiang
  - Minsexual pride flag
  - Minsexual pride flag (alt)
  - Moldavian SSR
  - Molossia
  - Monaco
  - Morocco
  - Murrawarri Republic
  - Sultanate of Muscat and Oman
  - Myanmar
  - Nauru
  - Nepal
  - Niger
  - Nigeria
  - Ninsexual pride flag
  - Ninsexual pride flag (alt)
  - Nonbinary pride flag
  - Naval ensign of Norway
  - Palau
  - Palestine
  - Pan-African flag
  - Panama
  - Pangender pride flag
  - Pansexual pride flag
  - Peru
  - Philippines
  - Polysexual pride flag
  - Pomerania
  - Puerto Rico
  - Qatar
  - Quebec
  - Red Cross
  - Redonda
  - Rhenish Republic
  - Rhine
  - Rhode Island
  - Russian SFSR
  - Rwanda (modern)
  - Rwanda (1959)
  - São Tomé and Príncipe
  - Saxony
  - Senegal
  - Serbia
  - Shanxi Clique
  - Sierra Leone
  - Somalia
  - South Africa
  - Sudan
  - Suriname
  - Flag of surrender
  - Naval ensign of Sweden
  - Switzerland (square)
  - Tajik SSR
  - Kingdom of Talossa
  - Talossan language
  - Tanganyika
  - Texas
  - Thin Blue Line (generic)
  - Thin Blue Line (Canada)
  - Thin Blue Line (Israel)
  - Thin Blue Line (USA)
  - Tonga
  - Trans pride flag
  - Pridnestrovian Moldavian Republic (Transnistria)
  - Trinidad and Tobago
  - Tunisia
  - Turkmen SSR
  - Uganda (modern)
  - Uganda (1962)
  - Ukrainian SSR
  - United Arab Emirates
  - Union of Soviet Socialist Republics (USSR) (alt)
  - Uzbek SSR
  - Vendsyssel
  - Venezuela
  - Republic of Vietnam
  - Socialist Republic of Vietnam
  - Washington, D.C.
  - Wirtland
  - Xibei San Ma
  - Xinjiang Clique
  - Yemen (modern)
  - Yemen Arab Republic (North Yemen)
  - South Yemen
  - Yugoslavia
  - Yugoslavia (socialist)
  - Yunnan Clique
  - Zanzibar and Pemba
</details>

[**Link to the Steam Workshop**](https://steamcommunity.com/sharedfiles/filedetails/?id=2047628891)  
[Link to Steam discussion page](https://steamcommunity.com/workshop/filedetails/discussion/729433195/2139714324745613701/)

### Wiki

This project has a work-in-progress wiki right [here](../../wikis/home)! Any other information, such as FAQs and more detailed documentation, will go there.

## Contributors

See [`CONTRIBUTING.md`](CONTRIBUTING.md) for information on how to contribute!

### Project manager: [Moofles](https://gitlab.com/MoofEMP)
&nbsp;&nbsp;&nbsp;&nbsp;Everything not otherwise credited below

### Flag designer: [catsanddogs333](https://steamcommunity.com/id/whoevencaresaboutids)
&nbsp;&nbsp;&nbsp;&nbsp;Soviet imagery, particularly [Flag of Transnistria](moreworldflagssubmod/objects/generic/newflagtransnistria)

### Flag designer: [Whisperwolf](https://gitlab.com/whisperwolf)
&nbsp;&nbsp;&nbsp;&nbsp;[Flag of Alabama](moreworldflagssubmod/objects/generic/newflagalabama)