### How to create a ref

1. Use a suitable ref in [`templates/`](templates) and draw the flag and its thumbnail inside the black outlines
2. Hide the layer with the black outlines and place the .pdn/.psd/.xcf in the appropriate subfolder in [`refs/`](refs)

### How to create a flag

IMPORTANT: None of this entails modifying [`templates/`](templates); don't change that
1. Create a directory [`moreworldflagssubmod/objects/generic/newflag[country]/`](moreworldflagssubmod/objects/generic)
2. Use the appropriate **sprite** in [`templates/`](templates) and [Flag Rippler for Paint.NET](../../../../../MoofEMP/pdn-flag-rippler) to create a flag sprite; name this newflag[country].png and save to the flag directory
3. Use the appropriate **thumb** in [`templates/`](templates) to create a thumbnail; name this newflag[country]icon.png and save to the flag directory
4. Move the ref you used to the appropriate subfolder in [`refs/archived/`](refs/archived)
5. Copy [`templates/objectbuilder.bat`](templates/objectbuilder.bat) to the flag directory, and run it to generate the .object and .frames files. Delete the new .bat afterward.
6. Copy [`templates/recipebuilder.bat`](templates/recipebuilder.bat) to [`moreworldflagssubmod/recipes/emptyhands/`](moreworldflagssubmod/recipes/emptyhands) and run it to generate the .recipe file. Delete the new .bat afterward.
    - For the purposes of categorising, consider Central America and the West Indies to be part of North America.
    - All Oceanic countries go in misc. There wasn't enough space for a seventh tab without making the UI really cramped, so this is a sacrifice that has to be made.
7. Finally, edit [`moreworldflagssubmod/player.config.patch`](moreworldflagssubmod/player.config.patch) and add a line just like the others with the name of your flag. Please keep this in alphabetical order.
    - Make sure that every line except the final one ends in a comma; failing to do so can cause a crash on startup.

*Why prefix with "newflag-" instead of "flag-"?*  
This is done for two reasons:  
&nbsp;&nbsp;&nbsp;&nbsp;a. To differentiate the flags in the original mod with the flags in the submod  
&nbsp;&nbsp;&nbsp;&nbsp;b. Just in case development restarts on the original mod, this minimises the likelihood of any conflicts.