# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Add
- Finish making refs & flags from [`checklist.txt`](checklist.txt)

### Change
- See if there's a way to make flags face right (not flipped) in the crafting UI

### Fix
- Do something about item names being so long they clip out of the UI
- Figure out if there's any way to avoid the crash caused by including the accents in "Côte d'Ivoire", "São Tomé and Príncipe", etc

## [0.4.7 Hotfix 1] - 2021-06-09
### Fixed
- Fixed missing newlines in some [v0.4.7](#047-2021-06-08) flags, which was causing their names to overflow in the crafting UI

## [0.4.7] - 2021-06-08
### Added
- <details>
  <summary>The following flags: (15 items)</summary>

  - Aroace pride flag
  - Bigender pride flag
  - Demiboy pride flag
  - Demigirl pride flag
  - Demiromantic pride flag
  - Demisexual pride flag
  - Fictosexual pride flag
  - Finsexual pride flag
  - Finsexual pride flag (alt)
  - Grey-asexual pride flag
  - Greyromantic pride flag
  - Minsexual pride flag
  - Minsexual pride flag (alt)
  - Ninsexual pride flag
  - Ninsexual pride flag (alt)
  - Pangender pride flag
</details>

### Changed
- QOL improvements to [`objectbuilder.bat`](../v0.4.7/templates/objectbuilder.bat) and [`recipebuilder.bat`](../v0.4.7/templates/recipebuilder.bat)
- Better organisation in [`templates/`](../v0.4.7/templates) and [`refs/archived/`](../v0.4.7/refs/archived)

## [0.4.6] - 2020-10-17
### Added
- Ref templates for pennants
- <details>
  <summary>The following flags: (33 items)</summary>
  
  - East Germany
  - Kuomintang Party
  - Maritime Signal Alfa
  - Maritime Signal Bravo
  - Maritime Signal Charlie
  - Maritime Signal Delta
  - Maritime Signal Echo
  - Maritime Signal Foxtrot
  - Maritime Signal Golf
  - Maritime Signal Hotel
  - Maritime Signal India
  - Maritime Signal Juliet
  - Maritime Signal Kilo
  - Maritime Signal Lima
  - Maritime Signal Mike
  - Maritime Signal November
  - Maritime Signal Oscar
  - Maritime Signal Papa
  - Maritime Signal Quebec
  - Maritime Signal Romeo
  - Maritime Signal Sierra
  - Maritime Signal Tango
  - Maritime Signal Uniform
  - Maritime Signal Victor
  - Maritime Signal Whiskey
  - Maritime Signal Xray
  - Maritime Signal Yankee
  - Maritime Signal Zulu
  - Maritime Signal First Substitute
  - Maritime Signal Second Substitute
  - Maritime Signal Third Substitute
  - Maritime Signal Fourth Substitute
  - Rhode Island
  </details>

## [0.4.5] - 2020-07-28
### Added
- <details>
  <summary>The following flags: (19 items)</summary>
  
  - Armenian SSR
  - Azerbaijan SSR
  - Byelorussian SSR
  - Estonian SSR
  - Georgian SSR
  - Karelo-Finnish SSR
  - Kazakh SSR
  - Kirghiz SSR
  - Latvian SSR
  - Lithuanian SSR
  - Moldavian SSR
  - Quebec
  - Russian SFSR
  - Tajik SSR
  - Transnistria
  - Turkmen SSR
  - Ukrainian SSR
  - USSR (alt)
  - Uzbek SSR
  </details>

## [0.4.4] - 2020-05-21
### Added
- Flag of Venezuela

## [0.4.3] - 2020-05-17
### Added
- Started creating [the wiki](../../wikis/home)!
- Contributors section in [`README.md`](../v0.4.3/README.md#contributors)
- <details>
  <summary>The following flags: (17 items)</summary>
  
  - Alabama
  - Alaska
  - Colorado
  - Jamaica
  - Kingdom of Talossa
  - Talossan language
  - Tanganyika
  - Thin Blue Line (generic)
  - Thin Blue Line (Canada)
  - Thin Blue Line (Israel)
  - Thin Blue Line (USA)
  - Tonga
  - Tunisia
  - United Arab Emirates
  - Washington, D.C.
  - Wirtland
  - Zanzibar and Pemba
  </details>

### Changed
- Freetown Christiania now uses "Christianian" as a demonym

## [0.4.2] - 2020-04-11
### Added
- <details>
  <summary>The following flags: (19 items)</summary>
  
  - Afghan Northern Alliance
  - Åland
  - Bornholm
  - Bornholm (alt)
  - Carnatic
  - Naval ensign of Denmark
  - Naval ensign of Iceland
  - Jutland
  - Maratha Empire
  - Mecklenburg
  - Mecklenburg-Vorpommern
  - Naval ensign of Norway
  - Pomerania
  - the Rhine
  - Rwanda (1959)
  - Saxony
  - South Africa
  - Naval ensign of Sweden
  - Vendsyssel
  </details>

## [0.4.1] - 2020-04-06
### Added
- <details>
  <summary>The following flags: (65 items)</summary>
  
  - Agender pride flag
  - Aromantic pride flag
  - Artsakh
  - Aruba
  - Asexual pride flag
  - Atlantium
  - Austenasia
  - Austrian Empire
  - Bisexual pride flag
  - Republic of China (1912)
  - Freetown Christiania
  - Cuba
  - Czech Republic (tricolour)
  - Djibouti
  - Elgaland-Vargaland
  - England
  - Faroe Islands
  - Free France
  - Genderfluid pride flag
  - Genderqueer pride flag
  - German Empire
  - Nazi Germany
  - Nazi Germany (alt)
  - Greece (alt)
  - Guernsey
  - Icaria
  - Intersex pride flag
  - Kingdom of Iraq
  - Kekistan
  - Ladonia
  - Lesbian pride flag
  - Malta
  - Manchukuo
  - Mengjiang
  - Molossia
  - Murrawarri Republic
  - Nauru
  - Nepal
  - Nonbinary pride flag
  - Pan-African flag
  - Panama
  - Pansexual pride flag
  - Peru
  - Philippines
  - Polysexual pride flag
  - Puerto Rico
  - Red Cross
  - Redonda
  - Rhenish Republic
  - Rwanda
  - Serbia
  - Flag of surrender
  - Switzerland (square)
  - Texas
  - Trans pride flag
  - Trinidad and Tobago
  - Republic of Vietnam
  - São Tomé and Príncipe
  - Socialist Republic of Vietnam
  - Xinjiang Clique
  - Yemen (modern)
  - Yemen Arab Republic (North Yemen)
  - South Yemen
  - Yugoslavia
  - Yugoslavia (socialist)
  </details>

## [0.4.0] - 2020-04-05
### Added
- Flags are now all in their own tabs sorted by continent! (Except Oceania, that got squashed into Misc)
- Added functionality to [`recipebuilder.bat`](../v0.4.0/templates/recipebuilder.bat) to choose the associated continent/category of a flag
- Flag of Croatia
- Flag of North Korea
- Flag of Uganda (modern)
- Flag of Uganda (1962)

### Changed
- Demonym for South Korea changed from "Korean" to "South Korean"

### Fixed
- A typo in the UI ("Crafing fancy flags!")

## [0.3.0] - 2020-04-04
### Added
- [Uploaded to the Steam Workshop!](https://steamcommunity.com/sharedfiles/filedetails/?id=2047628891)
- This means More World Flags is now officially in Beta!

### Changed
- Much prettier icon (using [Megrim](https://fonts.google.com/specimen/Megrim))
- Updated description field in [`_metadata`](../v0.3.0/moreworldflagssubmod/_metadata) to an abridged version of [`README.md`](../v0.3.0/README.md)

## [0.2.3] - 2020-04-03
### Added
- List of flags added to [`README.md`](../v0.2.3/README.md)
- <details>
  <summary>The following flags: (52 items)</summary>
  
  - Cambodia (modern)
  - Democratic Kampuchea (Communist Cambodia)
  - Cameroon
  - Cabo Verde
  - Central African Republic
  - Chad
  - Republic of China
  - Colombia
  - Comoros
  - Democratic Republic of the Congo
  - Republic of the Congo
  - Côte d'Ivoire
  - Estonia (nordic)
  - Gabon
  - Gambia
  - Georgia
  - Ghana
  - Greece
  - Guangxi Clique
  - Guinea
  - Guinea-Bissau
  - Holy Roman Empire
  - Iceland
  - Jordan
  - Kuwait
  - Laos
  - Latvia
  - LGBT pride flag
  - Libyan Arab Jamahiriya
  - Libya (modern)
  - Lithuania
  - Luxembourg
  - Madagascar
  - Mali
  - Mauritius
  - Monaco
  - Morocco
  - Sultanate of Muscat and Oman
  - Myanmar
  - Niger
  - Nigeria
  - Palau
  - Palestine
  - Qatar
  - Senegal
  - Shanxi Clique
  - Sierra Leone
  - Somalia
  - Sudan
  - Suriname
  - Xibei San Ma
  - Yunnan Clique
  </details>

### Changed
- Demonym for Bosnia and Herzegovina changed from "Bosnian and Herzegovinian" to simply "Bosnian"
- "Bahamas, Commonwealth of" changed to "Bahamas, Commonwealth of the"
- "Shanxi" changed to "Shanxi Clique"; "Yunnan" changed to "Yunnan Clique"

### Fixed
- Ref for Palestine was previously undocumented in [v0.1.0](#010-2020-03-31)

## [0.2.2] - 2020-04-01
### Added
- <details>
  <summary>The following flags: (19 items)</summary>

  - Angola
  - Antigua and Barbuda
  - Armenia
  - Azerbaijan
  - Bahrain
  - Bangladesh
  - Barbados
  - Belarus
  - Belize
  - Benin
  - Bhutan
  - Bolivia
  - Bosnia and Herzegovina (modern)
  - Republic of Bosnia and Herzegovina
  - Socialist Republic of Bosnia and Herzegovina
  - Botswana
  - Brunei
  - Burkina Faso
  - Burundi
</details>

### Changed
- flagbuilder.bat now references the input demonym instead of using generic examples during the article input (this is only a UI change)
- Pretty new icon with all sorts of different flags instead of only 4

## [0.2.1] - 2020-03-31
### Added
- Made a tutorial for ref and flag creation; that's in the [`README.md`](../v0.2.1/README.md) now
- Flag of Afghanistan
- Flag of Albania
- Flag of Algeria
- Flag of Andorra

### Changed
- <details>
  <summary>Updated localisation (ingame names) for the following flags: (21 items)</summary>

  - Argentina
  - Austria
  - Belgium
  - Brazil
  - Bulgaria
  - Chile
  - Denmark
  - Estonia
  - Finland
  - Indonesia
  - Ireland
  - Israel
  - North Macedonia
  - Mexico
  - Norway
  - Poland
  - Sweden
  - Switzerland
  - Thailand
  - Turkey
  - Uruguay
</details>

## [0.2.0] - 2020-03-31
### Added
- [Batch tool](../v0.2.0/templates/objectbuilder.bat) to automatically generate flag.object and flag.frames files
- [Another batch tool](../v0.2.0/templates/recipebuilder.bat) to automatically generate flag.recipe files

### Fixed
- Minor localisation fixes

## [0.1.0] - 2020-03-31
### Added
- Templates to help create more refs & sprites
- Localisation updates for all of the pre-existing flags ("Flag of X" instead of "X Flag" with full country names)
- Flag of the Bahamas
- <details>
  <summary>Refs for the following flags: (75 items)</summary>

  - Afghanistan
  - Albania
  - Algeria
  - Andorra
  - Angola
  - Antigua and Barbuda
  - Armenia
  - Azerbaijan
  - Bahrain
  - Bangladesh
  - Barbados
  - Belarus
  - Belize
  - Benin
  - Bhutan
  - Bolivia
  - Bosnia and Herzegovina (modern)
  - Bosnia and Herzegovina (republic)
  - Bosnia and Herzegovina (socialist)
  - Botswana
  - Brunei
  - Burkina Faso
  - Burundi
  - Cambodia (modern)
  - Cambodia (communist)
  - Cameroon
  - Cabo Verde
  - Central African Republic
  - Chad
  - Republic of China
  - Colombia
  - Comoros
  - Democratic Republic of the Congo
  - Republic of the Congo
  - Côte d'Ivoire
  - Estonia (nordic)
  - Gabon
  - Gambia
  - Georgia
  - Ghana
  - Greece
  - Guangxi Clique
  - Guinea
  - Guinea-Bissau
  - Holy Roman Empire
  - Iceland
  - Jordan
  - Kuwait
  - Laos
  - Latvia
  - Lithuania
  - LGBT pride flag (it's not a country, but it's well-known enough)
  - Libyan Arab Jamahiriya
  - Libya (modern)
  - Luxembourg
  - Madagascar
  - Mali
  - Mauritius
  - Monaco
  - Morocco
  - Sultanate of Muscat and Oman
  - Myanmar
  - Niger
  - Nigeria
  - Palau
  - Palestine
  - Qatar
  - Senegal
  - Shanxi Clique
  - Sierra Leone
  - Somalia
  - Sudan
  - Suriname
  - Xibei San Ma
  - Yunnan Clique
</details>